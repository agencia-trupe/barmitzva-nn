<ul class="nav navbar-nav">
    <li @if(str_is('painel.autorizacoes.*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.autorizacoes.index') }}">Autorizações</a>
    </li>
	<li @if(str_is('painel.arquivos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.arquivos.index') }}">Arquivos</a>
	</li>
</ul>
