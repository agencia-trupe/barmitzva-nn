<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <title>{{ config('site.name') }} - Painel Administrativo</title>

    {!! Tools::loadCss('painel/main.css') !!}
    {!! Tools::loadCss('vendor/datatables/media/css/dataTables.bootstrap.min.css') !!}
</head>
<body>
    @include('painel.common.header')

    <div class="container" style="padding-bottom:30px;">
        @yield('content')
    </div>

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('vendor/ckeditor/ckeditor.js') !!}
    {!! Tools::loadJs('painel/vendor.js') !!}
    {!! Tools::loadJs('painel/ajax-setup.js') !!}
    {!! Tools::loadJs('painel/delete-button.js') !!}
    {!! Tools::loadJs('painel/filtro.js') !!}
    {!! Tools::loadJs('painel/images-upload.js') !!}
    {!! Tools::loadJs('painel/order-images.js') !!}
    {!! Tools::loadJs('painel/order-table.js') !!}
    {!! Tools::loadJs('painel/text-editor.js') !!}
    {!! Tools::loadJs('painel/multi-select.js') !!}
    {!! Tools::loadJs('painel/data-tables.js') !!}
    {!! Tools::loadJs('painel/clipboard.js') !!}
</body>
</html>
