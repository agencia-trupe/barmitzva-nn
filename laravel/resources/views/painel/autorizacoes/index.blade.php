@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Autorizações</h2>
    </legend>

    @if(!count($autorizacoes))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered">
        <tbody>
        @foreach ($autorizacoes->chunk(2) as $chunk)
            <tr class="tr-row">
                @foreach($chunk as $autorizacao)
                <td>
                    <a href="{{ route('painel.autorizacoes.show', $autorizacao->id) }}">
                        {{ $autorizacao->filho_nome }}
                    </a>
                </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
