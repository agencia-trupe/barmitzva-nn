@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Autorizações</h2>
    </legend>

    <div class="form-group">
        <label>Nome completo do(a) filho(a):</label>
        <div class="well">{{ $autorizacao->filho_nome }}</div>
    </div>
    <div class="form-group">
        <label>RG do(a) filho(a):</label>
        <div class="well">{{ $autorizacao->filho_rg }}</div>
    </div>

    <div class="form-group">
        <label>Nome completo da mãe:</label>
        <div class="well">{{ $autorizacao->mae_nome }}</div>
    </div>
    <div class="form-group">
        <label>RG da mãe:</label>
        <div class="well">{{ $autorizacao->mae_rg }}</div>
    </div>

    <div class="form-group">
        <label>Nome completo do pai:</label>
        <div class="well">{{ $autorizacao->pai_nome }}</div>
    </div>
    <div class="form-group">
        <label>RG do pai:</label>
        <div class="well">{{ $autorizacao->pai_rg }}</div>
    </div>

    <a href="{{ route('painel.autorizacoes.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@endsection
