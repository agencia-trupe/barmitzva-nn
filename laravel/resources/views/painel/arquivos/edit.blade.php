@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Arquivos</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.arquivos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.arquivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
