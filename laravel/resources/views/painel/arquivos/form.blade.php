@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('ficha_de_saude', 'Ficha de Saúde') !!}
    @if($registro->ficha_de_saude)
    <p>
        <a href="{{ url('assets/pdfs/'.$registro->ficha_de_saude) }}" target="_blank" style="display:block;">{{ $registro->ficha_de_saude }}</a>
    </p>
    @endif
    {!! Form::file('ficha_de_saude', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('o_que_levar', 'O que levar') !!}
    @if($registro->o_que_levar)
    <p>
        <a href="{{ url('assets/pdfs/'.$registro->o_que_levar) }}" target="_blank" style="display:block;">{{ $registro->o_que_levar }}</a>
    </p>
    @endif
    {!! Form::file('o_que_levar', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('orientacoes_gerais', 'Orientações Gerais') !!}
    @if($registro->orientacoes_gerais)
    <p>
        <a href="{{ url('assets/pdfs/'.$registro->orientacoes_gerais) }}" target="_blank" style="display:block;">{{ $registro->orientacoes_gerais }}</a>
    </p>
    @endif
    {!! Form::file('orientacoes_gerais', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
