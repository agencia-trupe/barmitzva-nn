@extends('frontend.common.template')

@section('content')

    <div class="main autorizacao center">
        @if(session('success'))
            <p class="enviado">Autorização enviada com sucesso!</p>
        @else
            @if(count($errors) > 0)
            <p class="erro">Preencha todos os campos obrigatórios</p>
            @endif

            <form action="{{ route('autorizacao.post') }}" method="POST">
                {{ csrf_field() }}

                <p>Autorizamos nosso filho(a)</p>
                <input type="text" name="filho_nome" placeholder="nome completo do(a) filho(a)" value="{{ old('filho_nome') }}" @if($errors->has('filho_nome')) class="erro" @endif>
                <input type="text" name="filho_rg" placeholder="RG do(a) filho(a)" value="{{ old('filho_rg') }}" @if($errors->has('filho_rg')) class="erro" @endif>
                <p>a viajar desacompanhado(a) ao Acampamento NR nos dias 12, 13 e 14 de maio de 2017.</p>

                <p class="spaced">Mãe:</p>
                <input type="text" name="mae_nome" placeholder="nome completo da mãe" value="{{ old('mae_nome') }}" @if($errors->has('mae_nome')) class="erro" @endif>
                <input type="text" name="mae_rg" placeholder="RG da mãe" value="{{ old('mae_rg') }}" @if($errors->has('mae_rg')) class="erro" @endif>

                <p class="spaced">Pai:</p>
                <input type="text" name="pai_nome" placeholder="nome completo do pai" value="{{ old('pai_nome') }}" @if($errors->has('pai_nome')) class="erro" @endif>
                <input type="text" name="pai_rg" placeholder="RG do pai" value="{{ old('pai_rg') }}" @if($errors->has('pai_rg')) class="erro" @endif>

                <input type="submit" value="Enviar">
            </form>
        @endif
    </div>

@endsection
