@extends('frontend.common.template')

@section('content')

    <div class="convite-overlay"></div>
    <div class="convite">
        <img src="{{ asset('assets/img/layout/abertura.png') }}" alt="">
        <div class="seta">&raquo;</div>
    </div>

    <div class="banner-home">
        <div class="banners-slides">
            @foreach(range(1,3) as $i)
            <img src="{{ asset("assets/img/layout/foto-home${i}.jpg") }}" alt="">
            @endforeach
        </div>
    </div>

@endsection
