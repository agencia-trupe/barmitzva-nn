@extends('frontend.common.template')

@section('content')

    <div class="main mapa center">
        <img src="{{ asset('assets/img/layout/mapa.jpg') }}" alt="">
        <p>Ver mais instruções e mapas no site do NR:</p>
        <a href="http://nr.com.br/mapas/" target="_blank">Mais Instruções</a>
    </div>

@endsection
