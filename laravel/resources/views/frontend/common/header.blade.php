    <header @if(Tools::isActive('home')) class="home" @endif>
        <div class="center">
            <a href="{{ route('home') }}" class="topo">
                @if(Tools::isActive('home'))
                <img src="{{ asset('assets/img/layout/topo-branco.png') }}" alt="">
                @else
                <img src="{{ asset('assets/img/layout/topo-rosa.png') }}" alt="">
                @endif
            </a>

            <nav>
                <a href="{{ route('autorizacao') }}" @if(Route::currentRouteName() == 'autorizacao') class="active" @endif>Autorização de viagem</a>
                <a href="{{ route('saude') }}" @if(Route::currentRouteName() == 'saude') class="active" @endif>Ficha de Saúde</a>
                <a href="{{ route('levar') }}" @if(Route::currentRouteName() == 'levar') class="active" @endif>O que levar?</a>
                <a href="{{ route('mapa') }}" @if(Route::currentRouteName() == 'mapa') class="active" @endif>Mapa</a>
            </nav>
        </div>
    </header>
