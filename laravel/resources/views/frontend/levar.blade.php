@extends('frontend.common.template')

@section('content')

    <div class="main levar center">
        <div class="lista">
            <div class="col">
                <h2>Cama</h2>
                <ul>
                    <li>1 cobertor</li>
                    <li>1 fronha</li>
                    <li>2 lençóis ou saco de dormir</li>
                    <li>1 travesseiro</li>
                    <li>1 pijama</li>
                </ul>

                <h2>Vestuário diário</h2>
                <ul>
                    <li>5 camisetas</li>
                    <li>2 calças jeans</li>
                    <li>2 conjuntos de moleton (calça e blusa)</li>
                    <li>5 pares de meias</li>
                    <li>2 pares de tênis</li>
                    <li>5 roupas íntimas</li>
                    <li>1 casaco</li>
                    <li>1 roupa branca para a balada</li>
                    <li>1 camiseta do time de futebol</li>
                    <li>1 polo para Shabat - meninos / Similar - meninas</li>
                    <li>pijama para café da manhã de pijama</li>
                    <li>roupa branca que pode ser manchada de tinta</li>
                    <li>roupa para trilha seca ou na lama (camiseta, calça ou bermuda resistente, tênis, meia)</li>
                </ul>
            </div>

            <div class="col">
                <h2>Banho e piscina</h2>
                <ul>
                    <li>1 toalha de banho</li>
                    <li>1 toalha de rosto</li>
                    <li>1 toalha de piscina</li>
                    <li>1 sandália de borracha</li>
                    <li>2 maiôs ou biquínis</li>
                </ul>

                <h2>Material de uso pessoal</h2>
                <ul>
                    <li>1 lanterna com pilhas</li>
                    <li>1 saco para roupa suja</li>
                    <li>sabonete e saboneteira</li>
                    <li>pente ou escova de cabelos</li>
                    <li>creme dental e escova de dentes</li>
                    <li>shampoo e condicionador</li>
                    <li>desodorante</li>
                    <li>protetor solar</li>
                    <li>repelente</li>
                    <li>cantil/garrafa para água</li>
                </ul>
            </div>
        </div>

        <div class="links">
            <a href="{{ $arquivos->o_que_levar ? asset('assets/pdfs/'.$arquivos->o_que_levar) : '#' }}" target="_blank">Imprimir lista</a>
            <a href="{{ $arquivos->orientacoes_gerais ? asset('assets/pdfs/'.$arquivos->orientacoes_gerais) : '#' }}" target="_blank">Orientações gerais</a>
        </div>
    </div>

@endsection
