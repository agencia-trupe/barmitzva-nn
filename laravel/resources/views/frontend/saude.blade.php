@extends('frontend.common.template')

@section('content')

    <div class="main saude center">
        <a href="{{ $arquivos->ficha_de_saude ? asset('assets/pdfs/'.$arquivos->ficha_de_saude) : '#' }}" target="_blank">Imprimir ficha</a>
        <p>Esta ficha deverá ser preenchida em data próxima ao embarque para que as informações nela contidas estejam atualizadas.</p>
        <p>
            Preencher com letra legível, scanear e enviar para:
            <a href="mailto:rsvp@batnatashaenicole.com.br">rsvp@batnatashaenicole.com.br</a>
            juntamente com uma cópia simples do RG do aluno.
        </p>
        <p>Só embarcarão os alunos que tiverem apresentado:</p>
        <ul>
            <li><span>1.</span> Autorização de viagem preenchida via site</li>
            <li><span>2.</span> Ficha de saúde preenchida e enviada por e-mail</li>
            <li><span>3.</span> Cópia do RG do aluno scaneada e enviada por e-mail</li>
        </ul>
    </div>

@endsection
