(function() {
    'use strict';

    $('.banners-slides').cycle({
        timeout: 5000,
        speed: 800
    }).cycle('pause');
}());
