(function() {
    'use strict';

    var $convite = $('.convite'),
        $overlay = $('.convite-overlay');

    $convite.click(function(event) {
        event.preventDefault();

        $convite.addClass('animated slideOutUp');
        setTimeout(function() {
            $('body').addClass('home');
        }, 500);
        setTimeout(function() {
            $('.banners-slides').cycle('resume');
            $overlay.fadeOut();
            $('html, body').animate({ scrollTop: 0 }, '300');
        }, 1000);
    });
}());
