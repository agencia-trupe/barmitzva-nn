<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArquivosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ficha_de_saude'     => 'mimes:pdf',
            'o_que_levar'        => 'mimes:pdf',
            'orientacoes_gerais' => 'mimes:pdf',
        ];
    }
}
