<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\AutorizacoesRequest;
use App\Http\Controllers\Controller;

use Mail;

use App\Models\Autorizacao;
use App\Models\Arquivos;

class HomeController extends Controller
{
    public function __construct()
    {
        view()->share([
            'arquivos' => Arquivos::first()
        ]);
    }

    public function index()
    {
        return view('frontend.home');
    }

    public function autorizacao()
    {
        return view('frontend.autorizacao');
    }

    public function autorizacaoPost(AutorizacoesRequest $request)
    {
        Autorizacao::create($request->all());

        \Mail::send('emails.autorizacao', $request->all(), function($message) use ($request)
        {
            $message
                ->to(env('MAIL_AUTORIZACOES'), config('site.name'))
                ->from(env('MAIL_USERNAME'), config('site.name'))
                ->subject('Autorização de viagem');
        });

        return back()->with('success', true);
    }

    public function saude()
    {
        return view('frontend.saude');
    }

    public function levar()
    {
        return view('frontend.levar');
    }

    public function mapa()
    {
        return view('frontend.mapa');
    }
}
