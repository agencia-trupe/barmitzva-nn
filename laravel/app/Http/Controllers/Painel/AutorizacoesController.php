<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Autorizacao;

class AutorizacoesController extends Controller
{
    public function index()
    {
        $autorizacoes = Autorizacao::orderBy('filho_nome', 'ASC')->get();

        return view('painel.autorizacoes.index', compact('autorizacoes'));
    }

    public function show(Autorizacao $autorizacao)
    {
        return view('painel.autorizacoes.show', compact('autorizacao'));
    }
}
