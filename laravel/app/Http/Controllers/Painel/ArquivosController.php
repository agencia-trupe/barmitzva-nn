<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ArquivosRequest;
use App\Http\Controllers\Controller;

use App\Models\Arquivos;

class ArquivosController extends Controller
{
    public function index()
    {
        $registro = Arquivos::first();

        return view('painel.arquivos.edit', compact('registro'));
    }

    public function update(ArquivosRequest $request, Arquivos $registro)
    {
        try {
            $input = $request->all();

            if ($request->hasFile('ficha_de_saude')) {
                $input['ficha_de_saude'] = Arquivos::uploadFile('ficha_de_saude');
            }
            if ($request->hasFile('o_que_levar')) {
                $input['o_que_levar'] = Arquivos::uploadFile('o_que_levar');
            }
            if ($request->hasFile('orientacoes_gerais')) {
                $input['orientacoes_gerais'] = Arquivos::uploadFile('orientacoes_gerais');
            }

            $registro->update($input);

            return redirect()->route('painel.arquivos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
