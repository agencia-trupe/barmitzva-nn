exports.development = {
    vhost  : 'barmitzva-nn.dev',
    stylus : './resources/assets/stylus/',
    js     : './resources/assets/js/',
    img    : './resources/assets/img/',
    vendor : '../public/assets/vendor/'
};

exports.build = {
    css    : '../public/assets/css/',
    js     : '../public/assets/js/',
    img    : '../public/assets/img/layout/',
    painel : '../public/assets/painel/'
};

exports.vendor = [
];

exports.vendorPainel = [
    'bootswatch-dist/js/bootstrap.min.js',
    'jquery-ui/jquery-ui.min.js',
    'bootbox.js/bootbox.js',
    'blueimp-file-upload/js/jquery.fileupload.js',
    'jscolor-picker/jscolor.min.js',
    'toastr/toastr.min.js',
    'bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
    'datatables/media/js/jquery.dataTables.min.js',
    'datatables/media/js/dataTables.bootstrap.min.js',
    'clipboard/dist/clipboard.min.js'
];
