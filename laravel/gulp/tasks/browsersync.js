var config      = require('../config'),
    gulp        = require('gulp'),
    browserSync = require('browser-sync');

gulp.task('browserSync', function() {
    browserSync({
        proxy: config.development.vhost,
        open: false,
        notify: false
    });
});
