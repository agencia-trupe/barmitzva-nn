var config      = require('../config'),
    gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    plumber     = require('gulp-plumber');

gulp.task('vendorPainel', function() {
    return gulp.src(config.vendorPainel.map(function(path) {
        return config.development.vendor + path;
    }))
        .pipe(plumber())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(config.build.painel));
});
