var config      = require('../config'),
    gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    plumber     = require('gulp-plumber'),
    browserSync = require('browser-sync');

gulp.task('js', function() {
    return gulp.src([
        config.development.js + 'bootstrap.js',
        config.development.js + '**/*.js'
    ])
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.build.js))
        .pipe(browserSync.reload({ stream: true }))
});
