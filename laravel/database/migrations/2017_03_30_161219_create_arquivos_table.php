<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArquivosTable extends Migration
{
    public function up()
    {
        Schema::create('arquivos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ficha_de_saude');
            $table->string('o_que_levar');
            $table->string('orientacoes_gerais');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('arquivos');
    }
}
