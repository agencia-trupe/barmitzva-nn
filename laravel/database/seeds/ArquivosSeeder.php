<?php

use Illuminate\Database\Seeder;

class ArquivosSeeder extends Seeder
{
    public function run()
    {
        DB::table('arquivos')->insert([
            'ficha_de_saude' => '',
            'o_que_levar' => '',
            'orientacoes_gerais' => '',
        ]);
    }
}
