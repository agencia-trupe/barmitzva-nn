(function() {
    'use strict';

    $('.monthpicker').datepicker({
        changeMonth: true,
        changeYear: true,
        onClose: function() {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        },
        beforeShow: function() {
            var selDate = $(this).val();
            if (selDate.length > 0) {
                var year = selDate.substring(selDate.length - 4);
                var month = selDate.substring(0, 2);
                $(this).datepicker('option', 'defaultDate', new Date(year, month, 0))
                $(this).datepicker('setDate', new Date(year, month, 0));
            }
        },
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dateFormat: 'mm/yy',
    });

    $('html > head').append('<style>.ui-datepicker-calendar { display: none; }.ui-datepicker select.ui-datepicker-month,.ui-datepicker select.ui-datepicker-year{ color: #2C3E50; font-weight: normal; }</style>');
    if ($('.monthpicker').val() == '') $('.monthpicker').datepicker("setDate", new Date());
}());
